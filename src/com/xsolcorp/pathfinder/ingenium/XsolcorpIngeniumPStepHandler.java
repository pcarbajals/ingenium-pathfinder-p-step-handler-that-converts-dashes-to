/*******************************************************************************
 * All right, title and interest in and to the software (the "Software") and the 
 * accompanying documentation or materials (the "Documentation"),
 * including all proprietary rights, therein including all patent rights,
 * trade secrets, trademarks and copyrights, shall remain the exclusive        
 * property of Pablo A. Carbajal Siller.
 * No interest, license or any right respecting the Software and the 
 * Documentation is granted by implication or otherwise.                
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from 
 * the Pablo A. Carbajal Siller.
 *                                                               
 * (C) Copyright 2017 Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 *******************************************************************************/
package com.xsolcorp.pathfinder.ingenium;

import java.util.HashMap;
import java.util.Map;

import com.solcorp.ingenium.config.IngeniumConfig;
import com.solcorp.ingenium.gateway.GatewayException;
import com.solcorp.ingenium.gateway.Mir;
import com.solcorp.ingenium.gateway.MirElement;
import com.solcorp.ingenium.gateway.Sir;
import com.solcorp.ingenium.gateway.Transaction;
import com.solcorp.ingenium.resources.IngeniumMessages;
import com.solcorp.pathfinder.definitions.Attributes;
import com.solcorp.pathfinder.definitions.Definitions;
import com.solcorp.pathfinder.definitions.ExpressionValue;
import com.solcorp.pathfinder.definitions.GroupTableResolver;
import com.solcorp.pathfinder.definitions.Message;
import com.solcorp.pathfinder.definitions.MessagesEV;
import com.solcorp.pathfinder.definitions.PStepDef;
import com.solcorp.pathfinder.definitions.PStepHandler;
import com.solcorp.pathfinder.definitions.ProcessContext;
import com.solcorp.pathfinder.definitions.StepVariable;
import com.solcorp.pathfinder.definitions.StepVariableIterator;
import com.solcorp.pathfinder.definitions.StringEV;
import com.solcorp.pathfinder.definitions.StringTables;
import com.solcorp.pathfinder.definitions.TableValueEV;
import com.solcorp.pathfinder.definitions.VariableCollection;
import com.solcorp.pathfinder.definitions.Variables;
import com.solcorp.pathfinder.exceptions.InitializationException;
import com.solcorp.pathfinder.util.logging.Logger;

/**
 * This custom P-Step Handler behaves similar to PathFinder's
 * IngeniumPStepHander with the exception that it replaces some special
 * characters as required by the client due to a mismatches of page encoding
 * between Microsoft SQL database, Ingenium and PathFinder.
 * 
 * This P-Step handler performs the following conversions in the values of
 * fields going into Ingenium: - all instances of "en" (short) and "em" (long)
 * dashes into hyphens - all instances of left or right single quotation marks
 * to single quote
 * 
 * All other characters will remain the same.
 * 
 * Here are the steps for deploying the new P-Step handler:
 * 
 * <ol>
 * <li>Place the attached new XsolcorpIngeniumPStepHandler.jar file at the same
 * level as the Ingenium.jar file inside the PathFinderEar directory.</li>
 * <li>Update the PathFinderConfig.xml file from:
 * 
 * <pre>
 * {@code
 * <!-- The default PStepHandler implementation to execute P-Steps. -->
 * <PStepHandler> com.solcorp.ingenium.pathfinder.IngeniumPStepHandler </PStepHandler>
 * }
 * </pre>
 * 
 * to
 * 
 * <pre>
 * {@code
 * <!-- The default PStepHandler implementation to execute P-Steps. -->
 * <PStepHandler> com.xsolcorp.pathfinder.ingenium.XsolcorpIngeniumPStepHandler</PStepHandler>
 * }
 * </pre>
 * 
 * </li>
 * <li>Rebuild and redeploy the PathFinder EAR file</li>
 * </ol>
 * 
 */
public class XsolcorpIngeniumPStepHandler implements PStepHandler {
    private static final int HIGHEST_MESSAGE_SEVERITY = 5;
    private static final String INGENIUM_ERROR_RETURN_CODE = "01";
    private static final String INGENIUM_ERROR_RETURN_CODE_VARIABLE = "LSIR-RETURN-CD";
    private static final Logger LOG = Logger.getLogger(XsolcorpIngeniumPStepHandler.class);
    private boolean m_collectStatistics = false;
    private boolean m_ingeniumServerError = false;
    private final Map<String, PStepDef> m_pStepDefs = new HashMap<String, PStepDef>();
    private Transaction m_transaction = null;

    public XsolcorpIngeniumPStepHandler() throws InstantiationException {
	try {
	    this.m_transaction = new Transaction();
	} catch (InitializationException exc) {
	    throw new InstantiationException(exc.getMessage());
	}
    }

    public boolean addStep(PStepDef pStepDef, VariableCollection inVariables) {
	VariableCollection.Variable variable;
	this.m_transaction.setContext(pStepDef.getName());
	String mirName = pStepDef.getAttributeValue("MirName");
	if (mirName == null || mirName.trim().isEmpty()) {
	    LOG.error(IngeniumMessages.getMessage((String) "PF07-2081", new Object[] { pStepDef.getName() }));
	    return false;
	}
	this.m_pStepDefs.put(mirName, pStepDef);
	Mir mir = this.m_transaction.append(mirName);
	Sir sir = this.m_transaction.getSir();
	String businessFunctionId = pStepDef.getAttributeValue("BusinessFunctionId");
	if (businessFunctionId == null) {
	    LOG.error(IngeniumMessages.getMessage("PF07-2082", new Object[] { pStepDef.getName() }));
	    return false;
	}
	mir.setElementValue("MIR-BUS-FCN-ID", businessFunctionId);
	VariableCollection.VariableIterator iterator = inVariables.getVariableIterator();
	while ((variable = iterator.getNextVariable()) != null) {
	    String name = variable.getName();
	    ExpressionValue value = variable.getValue();
	    StepVariable stepVariable = pStepDef.getStepVariable(name);
	    if (stepVariable == null)
		continue;
	    if (!stepVariable.hasAttribute("MixedCase")) {
		value = this.convertToUpperCase(value);
	    }

	    value = convertSpecialCharacters(value);

	    if (name.startsWith("LSIR-")) {
		sir.setElementValue(name, value);
		continue;
	    }
	    mir.setElementValue(name, value);
	}
	return true;
    }

    /**
     * This method converts special characters to their equivalent character
     * that is accepted by Ingenium. Current special characters conversions are:
     * - en or em dashes to a hyphen - left or right single quotation marks to
     * single quote
     * 
     * @param value
     * @return
     */
    private ExpressionValue convertSpecialCharacters(ExpressionValue value) {
	String string;
	if (value.getType() == ExpressionValue.TABLE) {
	    TableValueEV table = (TableValueEV) value;
	    int size = table.size();
	    int count = 0;
	    while (count < size) {
		table.setValue(count, convertSpecialCharacters(table.getValue(count)));
		++count;
	    }
	} else if (value.getType() == ExpressionValue.STRING && (string = value.getString()).length() > 0) {
	    // replace all en and em dashes with a hyphen
	    String enOrEmDashRegex = "\\u2013|\\u2014";
	    String hyphen = "-";
	    string = string.replaceAll(enOrEmDashRegex, hyphen);

	    // replace all left and right single quotation marks with a single
	    // quote
	    String leftOrRightSingleQuotationRegex = "\\u2018|\\u2019";
	    String singleQuote = "'";
	    string = string.replaceAll(leftOrRightSingleQuotationRegex, singleQuote);

	    value = new StringEV(string);
	}

	return value;
    }

    public void collectStatistics(boolean collectStatistics) {
	this.m_collectStatistics = collectStatistics;
    }

    private ExpressionValue convertToUpperCase(ExpressionValue value) {
	String string;
	if (value.getType() == ExpressionValue.TABLE) {
	    TableValueEV table = (TableValueEV) value;
	    int size = table.size();
	    int count = 0;
	    while (count < size) {
		table.setValue(count, convertToUpperCase(table.getValue(count)));
		++count;
	    }
	} else if (value.getType() == ExpressionValue.STRING && (string = value.getString()).length() > 0) {
	    value = new StringEV(string.toUpperCase());
	}
	return value;
    }

    public MessagesEV getMessages() {
	Sir sir = this.m_transaction.getSir();
	TableValueEV messageTexts = (TableValueEV) sir.getElementValue("LSIR-USER-MSG-TEXT-T");
	if (messageTexts == null) {
	    return null;
	}
	MessagesEV messages = new MessagesEV();
	if (this.m_ingeniumServerError) {
	    Message message = new Message();
	    String messageText = StringTables.getInstance()
		    .getString(sir.getElementValue("LSIR-USER-LANG-CD").getString(), "IDS_ERROR_SERVER");
	    if (messageText == null) {
		messageText = "INGENIUM server error.  Check the PathFinder log file for details";
		LOG.error(IngeniumMessages.getMessage((String) "PF07-2083", (Object[]) new Object[0]));
	    }
	    message.setId("GATEWAY");
	    message.setContent(messageText);
	    message.setSeverity(HIGHEST_MESSAGE_SEVERITY);
	    messages.add(message);
	}
	TableValueEV messageSeverities = (TableValueEV) sir.getElementValue("LSIR-USER-MSG-SEVRTY-CD-T");
	TableValueEV messageIds = null;
	if (sir.hasElement("LSIR-USER-MSG-REF-INFO-T")) {
	    messageIds = (TableValueEV) sir.getElementValue("LSIR-USER-MSG-REF-INFO-T");
	}
	int index = 0;
	while (index < messageTexts.size()) {
	    ExpressionValue messageValue = messageTexts.getValue(index);
	    String messageText = messageValue.getString();
	    if (messageText.length() > 0) {
		Message message = new Message();
		message.setContent(messageText);
		if (messageIds != null) {
		    message.setId(messageIds.getValue(index).getString());
		}
		try {
		    message.setSeverity(Integer.parseInt(messageSeverities.getValue(index).getString()));
		} catch (NumberFormatException nfe) {
		    LOG.error(IngeniumMessages.getMessage((String) "PF07-2084", (Object[]) new Object[] { messageText,
			    messageSeverities.getValue(index).getString(), nfe.getMessage() }));
		    message.setSeverity(HIGHEST_MESSAGE_SEVERITY);
		}
		messages.add(message);
	    }
	    ++index;
	}
	ExpressionValue moreMessagesInd = sir.getElementValue("LSIR-MORE-MESSAGES-IND");
	messages.setMoreMessagesInd(moreMessagesInd.getString().equals("Y"));
	return messages;
    }

    private VariableCollection getOutValues() {
	VariableCollection outValues = new VariableCollection();
	Sir sir = this.m_transaction.getSir();
	for (final Mir mir : this.m_transaction.getMirs()) {
	    StepVariable stepVariable;
	    PStepDef pStepDef = this.m_pStepDefs.get(mir.getName());
	    StepVariableIterator iterator = pStepDef.getOutVariables(new GroupTableResolver() {

		@Override
		public int getGroupTableSize(String groupTableName) {
		    MirElement element = mir.getDescriptor().getElement(groupTableName);
		    if (element == null) {
			return 0;
		    }
		    return element.getOccurs();
		}
	    });

	    while ((stepVariable = iterator.getNextVariable()) != null) {
		String variableName = stepVariable.getName();
		try {
		    if (variableName.startsWith("LSIR-")) {
			outValues.setVariableValue(variableName, sir.getElementValue(variableName));
			continue;
		    }
		    outValues.setVariableValue(variableName, mir.getElementValue(variableName));
		    continue;
		} catch (VariableCollection.VariableReferenceException exc) {
		    LOG.error(IngeniumMessages.getMessage((String) "PF07-2010",
			    (Object[]) new Object[] { this.getClass().getName(), exc.getMessage() }));
		}
	    }
	}
	return outValues;
    }

    public ExpressionValue getSessionVariable(String name) {
	return this.m_transaction.getSir().getElementValue(name);
    }

    public VariableCollection invoke(ProcessContext processContext) {
	if (LOG.isDebugEnabled()) {
	    PStepDef pStepDef = processContext.getPStepDef();
	    LOG.debug("Executing PStepDef " + pStepDef.getName());
	    Attributes attributes = pStepDef.getAttributes();
	    if (attributes != null) {
		LOG.debug("Attributes: " + pStepDef.getAttributes().toString());
	    }
	}
	this.m_transaction.setContext(processContext.getProcessStackTrace());
	Sir sir = this.m_transaction.getSir();
	sir.setElementValue("LSIR-USER-SESN-ID", processContext.getSessionID());
	sir.setElementValue("LSIR-PRCES-ID", processContext.getProcessID());
	sir.setElementValue("LSIR-BPF-ID", processContext.getName());
	sir.setElementValue("LSIR-USER-LANG-CD", processContext.getLanguageID());
	if (this.m_collectStatistics) {
	    sir.setElementValue("LSIR-INIT-SERVER-STATS-IND", "Y");
	    if (sir.hasElement("LSIR-BAM-ID")) {
		sir.setElementValue("LSIR-BAM-ID", processContext.getBAMID());
	    }
	}
	try {
	    this.m_transaction.invoke(processContext.getUserID());
	} catch (GatewayException exc) {
	    String mirNames = this.m_transaction.getMirNames();
	    LOG.error(IngeniumMessages.getMessage((String) "PF07-2085", (Object[]) new Object[] { mirNames }));
	    LOG.error(IngeniumMessages.getMessage((String) "PF07-2086", (Object[]) new Object[] { exc.getMessage() }));
	    LOG.error(IngeniumMessages.getMessage((String) "PF07-2087", (Object[]) new Object[0]));
	    this.m_ingeniumServerError = true;
	    sir.setElementValue(INGENIUM_ERROR_RETURN_CODE_VARIABLE, INGENIUM_ERROR_RETURN_CODE);
	    VariableCollection outValues = new VariableCollection();
	    try {
		outValues.setVariableValue(INGENIUM_ERROR_RETURN_CODE_VARIABLE,
			(ExpressionValue) new StringEV(INGENIUM_ERROR_RETURN_CODE));
	    } catch (VariableCollection.VariableReferenceException exc1) {
		LOG.error(IngeniumMessages.getMessage((String) "PF07-2010",
			(Object[]) new Object[] { this.getClass().getName(), exc.getMessage() }));
	    }
	    return outValues;
	}
	long serverTime = (long) (this.m_transaction.getSir().getServerTaskDur() * 1000.0);
	processContext.setServerDuration(serverTime);
	processContext.setRPCDuration(this.m_transaction.getGatewayTime());
	return this.getOutValues();
    }

    public void notInFlow(Variables variables) {
	String securityStep = IngeniumConfig.getInstance()
		.getString("Ingenium.PathFinder.Definitions.IngeniumPStepFunctionID");
	PStepDef pStepDef = Definitions.getInstance().getPStepDef(securityStep);
	if (pStepDef == null) {
	    LOG.error(IngeniumMessages.getMessage((String) "PF07-2088", (Object[]) new Object[] { securityStep }));
	} else {
	    this.addStep(pStepDef, pStepDef.getInValues(variables));
	}
    }

    public void setSessionVariable(String name, ExpressionValue value) {
	Sir sir = this.m_transaction.getSir();
	if (sir != null) {
	    sir.setElementValue(name, value);
	} else {
	    LOG.debug("m_transaction.getSir() returned null");
	}
    }

    public String toString() {
	return "IngeniumPStepHandler()";
    }
}
